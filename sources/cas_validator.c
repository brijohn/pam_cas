/*
 *  Copyright (c) 2000-2003 Yale University. All rights reserved.
 *
 *  THIS SOFTWARE IS PROVIDED "AS IS," AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, ARE EXPRESSLY
 *  DISCLAIMED. IN NO EVENT SHALL YALE UNIVERSITY OR ITS EMPLOYEES BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED, THE COSTS OF
 *  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA OR
 *  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 *  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED IN ADVANCE OF THE POSSIBILITY OF SUCH
 *  DAMAGE.
 *
 *  Redistribution and use of this software in source or binary forms,
 *  with or without modification, are permitted, provided that the
 *  following conditions are met:
 *
 *  1. Any redistribution must include the above copyright notice and
 *  disclaimer and this list of conditions in any related documentation
 *  and, if feasible, in the redistributed software.
 *
 *  2. Any redistribution must include the acknowledgment, "This product
 *  includes software developed by Yale University," in any related
 *  documentation and, if feasible, in the redistributed software.
 *
 *  3. The names "Yale" and "Yale University" must not be used to endorse
 *  or promote products derived from this software.
 */

/*
 * CAS 2.0 service- and proxy-ticket validator in C, using OpenSSL.
 *
 * Originally by Shawn Bayern, Yale ITS Technology and Planning.
 * Patches submitted by Vincent Mathieu, University of Nancy, France.
 */
/*
 *
 * modify by esup consortium : http://esup-portail.org/
 * 
 */

#include <stdio.h>
#include <string.h>
#include <syslog.h>
#include "cas.h"
#include "xml.h"


#define END(x) { ret = (x); goto end; }
#define FAIL END(CAS_ERROR)
#define SUCCEED END(CAS_SUCCESS)


#define DEBUG_LOG(X, Y)  do { if (debug) { \
  if (debug == DEBUG_LOCAL) printf((X), (Y)); \
  else if (debug == DEBUG_SYSLOG) syslog(LOG_DEBUG, (X), (Y)); \
} \
} while (0);


static int debug = 0;

static int arrayContains(char *array[], char *element);


/** Returns status of ticket by filling 'buf' with a NetID if the ticket
 *  is valid and buf is large enough and returning 1.  If not, 0 is
 *  returned.
 */
int cas_proxy(
    char *ticket, char *service, char *outbuf, int outbuflen, pam_cas_config_t *config)
{
  int ret;
  char buf[4096];
  char *request = NULL, *str;
  char pt[256];
  char parsebuf[128];

  debug = config->debug;

  /* build request */
  request = malloc(strlen(config->uriProxy) + strlen("?pgt=") + strlen(ticket)
    + strlen("&targetService=") + strlen(service) + 1);
  if (request == NULL)
  {
      DEBUG_LOG("Error memory allocation%s\n", "");
      END(CAS_ERROR_MEMORY_ALLOC);
  }
  sprintf(request, "%s?pgt=%s&targetService=%s", config->uriProxy, ticket, service);

  ret = connection_get_url(config, request, buf, 4096);
  if (ret != CAS_SUCCESS)
    goto end;

  str = (char *)strstr(buf, "\r\n\r\n");  // find the end of the header

  if (!str)
  {
    DEBUG_LOG("no header in response%s\n", "");
    END(CAS_ERROR_HTTP);			  // no header
  }

  /*

   * 'str' now points to the beginning of the body, which should be an

   * XML document

   */

  // make sure that the authentication succeeded
  
  if (!element_body(
    str, "cas:proxySuccess", 1, parsebuf, sizeof(parsebuf))) {
    END(CAS_BAD_TICKET);
  }

  // retrieve the NetID
  if (!element_body(str, "cas:proxyTicket", 1, pt, sizeof(pt))) {
    DEBUG_LOG("unable to determine proxyTicket%s\n", "");
    END(CAS_PROTOCOL_FAILURE);
  }

  ret = cas_validate(pt, service, outbuf, outbuflen, config);
  if (ret != CAS_SUCCESS)
    goto end;

  SUCCEED;

   /* cleanup and return */

end:
  if (request)
    free(request);
  return ret;

}
/** Returns status of ticket by filling 'buf' with a NetID if the ticket
 *  is valid and buf is large enough and returning 1.  If not, 0 is
 *  returned.
 */
int cas_validate(
    char *ticket, char *service, char *outbuf, int outbuflen, pam_cas_config_t *config)
{
  int ret;
  char buf[4096];
  char *request = NULL, *str;
  char netid[CAS_LEN_NETID];
  char parsebuf[128];

  debug = config->debug;

  /* build request */
  request = malloc(strlen(config->uriValidate) + strlen("?ticket=") + strlen(ticket)
    + strlen("&service=") + strlen(service) + 1);
  if (request == NULL)
  {
      DEBUG_LOG("Error memory allocation%s\n", "");
      END(CAS_ERROR_MEMORY_ALLOC);
  }
  sprintf(request, "%s?ticket=%s&service=%s", config->uriValidate, ticket, service);

  ret = connection_get_url(config, request, buf, 4096);
  if (ret != CAS_SUCCESS)
    goto end;

  str = (char *)strstr(buf, "\r\n\r\n");  // find the end of the header

  if (!str)
  {
    DEBUG_LOG("no header in response%s\n", "");
    END(CAS_ERROR_HTTP);			  // no header
  }

  /*
   * 'str' now points to the beginning of the body, which should be an
   * XML document
   */

  // make sure that the authentication succeeded
  
  if (!element_body(
    str, "cas:authenticationSuccess", 1, parsebuf, sizeof(parsebuf))) {
    END(CAS_BAD_TICKET);
  }

  // retrieve the NetID
  if (!element_body(str, "cas:user", 1, netid, sizeof(netid))) {
    DEBUG_LOG("unable to determine username%s\n", "");
    END(CAS_PROTOCOL_FAILURE);
  }


  // check the first proxy (if present)
  if ((config->proxies) && (config->proxies[0]))
    if (element_body(str, "cas:proxies", 1, parsebuf, sizeof(parsebuf)))
      if (element_body(str, "cas:proxy", 1, parsebuf, sizeof(parsebuf)))
        if (!arrayContains(config->proxies, parsebuf)) {
          DEBUG_LOG("bad proxy: %s\n", parsebuf);
          END(CAS_BAD_PROXY);
        }

  /*
   * without enough space, fail entirely, since a partial NetID could
   * be dangerous
   */
  if (outbuflen < strlen(netid) + 1) 
  {
    syslog(LOG_ERR, "output buffer too short");
    DEBUG_LOG("output buffer too short%s\n", "");
    END(CAS_PROTOCOL_FAILURE);
  }

  strcpy(outbuf, netid);
  SUCCEED;

   /* cleanup and return */

end:
  if (request)
    free(request);
  return ret;
}

// returns 1 if a char* array contains the given element, 0 otherwise
static int arrayContains(char *array[], char *element) {
  char *p;
  int i = 0;

  for (p = array[0]; p; p = array[++i]) {
    DEBUG_LOG("  checking element %s\n", p);
    if (!strcmp(p, element))
      return 1;
  }
  return 0;
}
